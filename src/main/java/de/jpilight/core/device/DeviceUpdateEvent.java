package de.jpilight.core.device;

import de.jpilight.core.event.Event;

import java.util.Map;

public class DeviceUpdateEvent implements Event {

    private final String deviceName;
    private final Map<String, String> values;

    public DeviceUpdateEvent(String deviceName, Map<String, String> values) {
        this.deviceName = deviceName;
        this.values = values;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public Map<String, String> getValues() {
        return values;
    }

}
