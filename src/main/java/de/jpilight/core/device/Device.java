package de.jpilight.core.device;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;

/**
 * Base implementation of a Pilight device.
 * @param <T> The type of observable value that an observer receives on an update of this device
 */
public abstract class Device<T> {

    private Set<Consumer<T>> observers = new HashSet<>();

    private String name;

    /**
     * Get the name of this device. It is the same name as configured in pilight
     * @return
     */
    public String getName() {
        return this.name;
    }

    protected void setName(String name) {
        this.name = name;
    }

    protected abstract void update(Map<String, String> values);

    protected Set<Consumer<T>> getObservers() {
        return this.observers;
    }

    /**
     * If the values of this device change, the given Observer will be notified
     * @param observer
     */
    public void addObserver(Consumer<T> observer) {
        this.observers.add(observer);
    }

    /**
     * get the name of this device as string
     * @see Device#getName()
     */
    @Override
    public String toString() {
        return this.getName();
    }

}
