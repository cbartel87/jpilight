package de.jpilight.core.device;

import com.google.inject.Inject;
import com.google.inject.Provider;
import de.jpilight.api.device.TemperatureSensor;

public class TemperatureSensorFactory implements DeviceFactory {

    private static final String[] PROTOCOL_NAMES = {"ds18b20"};

    @Inject
    private Provider<TemperatureSensor> temperatureSensorProvider;

    @Override
    public Device createDevice(DeviceDefinition deviceDefinition) {
        return temperatureSensorProvider.get();
    }

    @Override
    public String[] getProtocolNames() {
        return PROTOCOL_NAMES;
    }
}
