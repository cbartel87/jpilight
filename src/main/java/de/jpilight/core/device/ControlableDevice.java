package de.jpilight.core.device;

import com.google.inject.Inject;
import de.jpilight.core.bus.Bus;
import de.jpilight.core.event.Event;

import java.util.Map;

public abstract class ControlableDevice<T> extends Device<T> {

    @Inject
    private Bus<Event> eventBus;

    protected void sendControl(String state, Map<String, Object> values) {
        DeviceControlEvent event = new DeviceControlEvent(this.getName(), state, values);
        this.eventBus.post(event);
    }

    protected void sendControl(String state)  {
        this.sendControl(state, null);
    }

}
