package de.jpilight.core.device;

import com.google.inject.Inject;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class DeviceRegistryImpl implements DeviceRegistry {

    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(DeviceRegistryImpl.class);

    private Map<String, DeviceFactory> deviceFactoryMap;
    private Map<String, Device> deviceMap;

    @Inject
    public DeviceRegistryImpl(Set<DeviceFactory> deviceFactories) {
        this.deviceFactoryMap = new HashMap<>();
        this.deviceMap = new HashMap<>();
        deviceFactories.forEach(this::initDeviceFactory);
    }

    private void initDeviceFactory(DeviceFactory deviceFactory) {
        for(String protocolName : deviceFactory.getProtocolNames()) {
            deviceFactoryMap.put(protocolName, deviceFactory);
        }
    }

    @Override
    public void registerDevice(DeviceDefinition definition) {
        for(String protocolName : definition.getProtocols()) {
            DeviceFactory deviceFactory = deviceFactoryMap.get(protocolName);
            if(deviceFactory != null) {
                Device device = deviceFactory.createDevice(definition);
                device.setName(definition.getName());
                deviceMap.put(definition.getName(), device);
                LOGGER.info("Registered Device {}. Protocols: {}", definition.getName(), definition.getProtocols());
                return;
            }
        }
    }

    @Override
    public <T extends Device> T getDevice(String name, Class<T> deviceType) {
        Device device = this.deviceMap.get(name);
        if(device == null || !deviceType.isAssignableFrom(device.getClass())) {
            return null;
        }
        return deviceType.cast(device);
    }

    @Override
    public Collection<Device> getAllDevices() {
        return deviceMap.values();
    }
}
