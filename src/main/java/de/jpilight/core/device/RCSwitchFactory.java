package de.jpilight.core.device;

import com.google.inject.Inject;
import com.google.inject.Provider;
import de.jpilight.api.device.RCSwitch;

public class RCSwitchFactory implements DeviceFactory {

    private static final String[] PROTOCOL_NAMES = {"kaku_switch"};

    @Inject
    private Provider<RCSwitch> rcSwitchProvider;

    @Override
    public Device createDevice(DeviceDefinition deviceDefinition) {
        return rcSwitchProvider.get();
    }

    @Override
    public String[] getProtocolNames() {
        return PROTOCOL_NAMES;
    }
}
