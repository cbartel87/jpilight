package de.jpilight.core.device;

import java.util.Collections;
import java.util.List;

public class DeviceDefinition {

    private final String name;
    private final List<String> protocols;

    public DeviceDefinition(String name, List<String> protocols) {
        this.name = name;
        this.protocols = Collections.unmodifiableList(protocols);
    }

    public String getName() {
        return name;
    }

    public List<String> getProtocols() {
        return protocols;
    }

    @Override
    public String toString() {
        return "DeviceDefinition{" +
                "name='" + name + '\'' +
                ", protocols=" + protocols +
                '}';
    }
}
