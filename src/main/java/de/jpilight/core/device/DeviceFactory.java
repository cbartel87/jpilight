package de.jpilight.core.device;

public interface DeviceFactory {

    Device createDevice(DeviceDefinition deviceDefinition);

    String[] getProtocolNames();
}
