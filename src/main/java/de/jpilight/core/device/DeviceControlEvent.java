package de.jpilight.core.device;

import de.jpilight.core.event.Event;

import java.util.Map;

public class DeviceControlEvent implements Event {

    private final String deviceName;
    private final String state;
    private final Map<String, Object> values;

    public DeviceControlEvent(String deviceName, String state, Map<String, Object> values) {
        this.deviceName = deviceName;
        this.state = state;
        this.values = values;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public String getState() {
        return state;
    }

    public Map<String, Object> getValues() {
        return values;
    }

}
