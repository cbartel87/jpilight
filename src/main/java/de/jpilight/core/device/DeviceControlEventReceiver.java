package de.jpilight.core.device;

import com.google.common.eventbus.Subscribe;
import com.google.inject.Inject;
import de.jpilight.core.bus.Bus;
import de.jpilight.core.event.Event;
import de.jpilight.core.device.DeviceControlEvent;
import de.jpilight.core.json.ControlJsonModel;

import java.util.HashMap;
import java.util.Map;

public class DeviceControlEventReceiver {

    @Inject
    private ControlJsonModel controlJsonModel;

    @Inject
    public DeviceControlEventReceiver(Bus<Event> eventBus) {
        eventBus.register(this);
    }

    @Subscribe
    public void onDeviceControlEvent(DeviceControlEvent event) {
        Map<String, Object> code = new HashMap<>();
        code.put("device", event.getDeviceName());
        code.put("state", event.getState());
        if(event.getValues() != null) {
            code.put("values", event.getValues());
        }
        controlJsonModel.code = code;
        controlJsonModel.send();
    }
}
