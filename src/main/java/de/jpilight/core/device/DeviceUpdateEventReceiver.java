package de.jpilight.core.device;

import com.google.common.eventbus.Subscribe;
import com.google.inject.Inject;
import de.jpilight.core.bus.Bus;
import de.jpilight.core.event.Event;

public class DeviceUpdateEventReceiver {

    @Inject
    private DeviceRegistry deviceRegistry;

    @Inject
    public DeviceUpdateEventReceiver(Bus<Event> eventBus) {
        eventBus.register(this);
    }

    @Subscribe
    public void onDeviceUpdateEvent(DeviceUpdateEvent event) {
        Device device = this.deviceRegistry.getDevice(event.getDeviceName(), Device.class);
        if(device != null) {
            device.update(event.getValues());
        }
    }
}
