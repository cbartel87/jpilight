package de.jpilight.core.device;

import java.util.Collection;

public interface DeviceRegistry {

    void registerDevice(DeviceDefinition definition);

    <T extends Device> T getDevice(String name, Class<T> deviceType);

    Collection<Device> getAllDevices();

}
