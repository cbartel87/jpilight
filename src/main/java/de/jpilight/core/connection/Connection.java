package de.jpilight.core.connection;

import java.io.IOException;
import java.net.SocketAddress;
import java.util.function.Consumer;

public interface Connection {
    void connect(SocketAddress address) throws IOException;

    void connect(String host, Integer port) throws IOException;

    void close();

    void write(String message);

    void addObserver(Consumer<String> consumer);
}
