package de.jpilight.core.connection.message;

public class RawMessage implements Message {

    private final String text;

    public RawMessage(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

}
