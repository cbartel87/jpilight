package de.jpilight.core.connection;

import de.jpilight.core.connection.channel.SocketReadChannel;
import de.jpilight.core.connection.channel.SocketWriteChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.function.Consumer;

public class SocketConnection implements Connection {

    private final static Logger logger = LoggerFactory.getLogger(SocketConnection.class);

    protected int CONNECTION_TIMEOUT = 10000;

    private Socket socket;
    protected SocketReadChannel socketReadChannel;
    protected SocketWriteChannel socketWriteChannel;

    protected SocketConnection() {
        this.socket = new Socket();
        this.socketReadChannel = new SocketReadChannel(socket);
        this.socketWriteChannel = new SocketWriteChannel(socket);
        this.socketReadChannel.onClose(this::close);
    }

    @Override
    public void connect(SocketAddress address) throws IOException {
        this.socket.connect(address, CONNECTION_TIMEOUT);
        this.socketWriteChannel.open();
        this.socketReadChannel.open();
        logger.info("Connected to {}", address.toString());
    }

    @Override
    public void connect(String host, Integer port) throws IOException {
        this.connect(new InetSocketAddress(host, port));
    }

    @Override
    public void close() {
        try {
            socket.close();
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        } finally {
            socketReadChannel.close();
            socketWriteChannel.close();
        }
    }

    @Override
    public void write(String message) {
        socketWriteChannel.write(message);
    }

    @Override
    public void addObserver(Consumer<String> consumer) {
        socketReadChannel.addObserver(consumer);
    }

}
