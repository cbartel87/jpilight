package de.jpilight.core.connection;

import com.google.common.eventbus.Subscribe;
import com.google.inject.Inject;
import de.jpilight.core.bus.Bus;
import de.jpilight.core.connection.event.ConnectedEvent;
import de.jpilight.core.event.Event;
import de.jpilight.core.json.IdentifyJsonModel;
import de.jpilight.core.json.RequestConfigJsonModel;

public class ConnectedEventReceiver {

    @Inject
    private IdentifyJsonModel identifyJsonModel;

    @Inject
    private RequestConfigJsonModel requestConfigJsonModel;

    @Inject
    public ConnectedEventReceiver(Bus<Event> eventBus) {
        eventBus.register(this);
    }

    @Subscribe
    public void onConnectedEvent(ConnectedEvent event) {
        identifyJsonModel.options.put("config", 1);
        identifyJsonModel.send();
        requestConfigJsonModel.send();
    }

}
