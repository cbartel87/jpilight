package de.jpilight.core.connection.channel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

public class SocketWriteChannel extends Channel<String> {

    private final static Logger logger = LoggerFactory.getLogger(SocketWriteChannel.class);

    protected final static String DEFAULT_ENCODING = "UTF-8";

    private BlockingQueue<String> inputBlockingQueue = new LinkedBlockingQueue<>();
    private PrintStream socketPrintStream;

    private String encoding;
    private Socket socket;

    public SocketWriteChannel(Socket socket, String encoding) {
        this.socket = socket;
        this.encoding = encoding;
    }

    public SocketWriteChannel(Socket socket) {
        this(socket, DEFAULT_ENCODING);
    }

    public void write(String message) {
        try {
            inputBlockingQueue.put(message);
        } catch (InterruptedException e) {
            logger.error(e.getMessage(), e);
        }
    }

    @Override
    public void open() {
        try {
            socketPrintStream = new PrintStream(socket.getOutputStream(), false, encoding);
            super.open();
        } catch(IOException e) {
            logger.error(e.getMessage(), e);
        }
    }

    @Override
    protected String read() {
        try {
            return inputBlockingQueue.poll(500, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            logger.error(e.getMessage(), e);
            return null;
        }
    }

    @Override
    protected void handle(String object) {
        if(object != null) {
            socketPrintStream.println(object);
            socketPrintStream.flush();
        }
    }


}
