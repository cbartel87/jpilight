package de.jpilight.core.connection.channel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class SocketReadChannel extends ObservableChannel<String> {

    private final static Logger logger = LoggerFactory.getLogger(SocketReadChannel.class);

    protected final static String DEFAULT_ENCODING = "UTF-8";
    protected final static int DEFAULT_READ_BYTES = 1025;

    private BufferedReader reader;
    private String encoding;
    private int readBytes;
    private Socket socket;

    public SocketReadChannel(Socket socket, String encoding, int readBytes) {
        this.socket = socket;
        this.encoding = encoding;
        this.readBytes = readBytes;
    }

    public SocketReadChannel(Socket socket) {
        this(socket, DEFAULT_ENCODING, DEFAULT_READ_BYTES);
    }

    @Override
    public void open() {
        try {
            InputStreamReader inputStreamReader = new InputStreamReader(socket.getInputStream(), encoding);
            this.reader = new BufferedReader(inputStreamReader, readBytes);
            super.open();
        } catch(IOException e) {
            logger.error(e.getMessage(), e);
        }
    }

    @Override
    protected String read() {
        try {
            return reader.readLine();
        } catch (IOException e) {
            this.close();
            return null;
        }
    }
}
