package de.jpilight.core.connection.channel;

import java.util.HashSet;
import java.util.Set;

public abstract class Channel<T> {

    private boolean isClosed = true;

    private Set<Runnable> closeActions = new HashSet<>();

    public void open() {
        isClosed = false;
        Thread thread = new Thread(this::loop);
        thread.setName(this.getClass().getSimpleName());
        thread.start();
    }

    private void loop() {
        while(!isClosed) {
            T object = this.read();
            handle(object);
        }
    }

    public void close() {
        if(isClosed) {
            return;
        }
        isClosed = true;
        closeActions.forEach(Runnable::run);
    }

    public void onClose(Runnable action) {
        closeActions.add(action);
    }

    protected abstract T read();

    protected abstract void handle(T object);
}
