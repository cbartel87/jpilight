package de.jpilight.core.connection.channel;

import java.util.HashSet;
import java.util.Set;
import java.util.function.Consumer;

public abstract class ObservableChannel<T> extends Channel<T> {

    private Set<Consumer<T>> observerSet = new HashSet<>();

    public void addObserver(Consumer<T> consumer) {
        this.observerSet.add(consumer);
    }

    @Override
    protected void handle(T object) {
        observerSet.stream().forEach(observer -> observer.accept(object));
    }

}
