package de.jpilight.core.connection;

import com.google.inject.Inject;
import de.jpilight.core.connection.event.ConnectedEvent;
import de.jpilight.core.event.Event;
import de.jpilight.core.connection.event.DisconnectedEvent;
import de.jpilight.core.bus.Bus;
import de.jpilight.core.connection.message.Message;
import de.jpilight.core.connection.message.RawMessage;

import java.io.IOException;
import java.net.SocketAddress;

public class JPilightSocketConnection extends SocketConnection {

    private StringBuilder messageBuilder = new StringBuilder();

    @Inject
    protected Bus<Message> messageBus;

    @Inject
    protected Bus<Event> eventBus;

    private boolean closed = false;

    public JPilightSocketConnection() {
        super();
        this.addObserver(this::handleChunk);
    }

    protected void handleChunk(String chunk) {
        if(chunk == null) {
            return;
        }
        if(chunk.isEmpty()) {
            messageBus.post(new RawMessage(messageBuilder.toString()));
            messageBuilder = new StringBuilder();
            return;
        }
        messageBuilder.append(chunk);
    }

    @Override
    public void connect(SocketAddress address) throws IOException {
        super.connect(address);
        this.closed = false;
        eventBus.post(new ConnectedEvent());
    }

    @Override
    public void close() {
        super.close();

        //prevent from DisconnectedEvent being called twice
        if(!closed) {
            eventBus.post(new DisconnectedEvent());
            closed = true;
        }
    }


}
