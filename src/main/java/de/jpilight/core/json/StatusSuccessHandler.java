package de.jpilight.core.json;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.inject.Inject;

public class StatusSuccessHandler implements JsonMessageHandler {



    @Inject
    private RequestConfigJsonModel requestConfigJsonModel;

    @Override
    public boolean canHandle(JsonObject object) {
        if(!object.has("status")) {
            return false;
        }
        JsonElement jsonElement = object.get("status");
        if(!jsonElement.isJsonPrimitive()) {
            return false;
        }
        String status = jsonElement.getAsString();
        return status.equals("success");
    }

    @Override
    public void handle(JsonObject object) {
        //TODO
    }

}
