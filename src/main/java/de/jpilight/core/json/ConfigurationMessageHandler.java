package de.jpilight.core.json;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.inject.Inject;
import de.jpilight.core.bus.Bus;
import de.jpilight.core.connection.event.ConfiguredEvent;
import de.jpilight.core.event.Event;
import de.jpilight.core.json.JsonMessageHandler;
import de.jpilight.core.device.DeviceDefinition;
import de.jpilight.core.device.DeviceRegistry;

import java.util.*;

public class ConfigurationMessageHandler implements JsonMessageHandler {

    @Inject
    private DeviceRegistry deviceRegistry;

    @Inject
    private Bus<Event> eventBus;

    @Override
    public boolean canHandle(JsonObject object) {
        if(!object.has("message")) {
            return false;
        }
        JsonElement jsonElement = object.get("message");
        if(!jsonElement.isJsonPrimitive()) {
            return false;
        }
        String message = jsonElement.getAsString();
        return message.equals("config");
    }

    @Override
    public void handle(JsonObject object) {
        JsonObject configJsonObject = object.get("config").getAsJsonObject();
        JsonObject devicesJsonObject = configJsonObject.get("devices").getAsJsonObject();
        devicesJsonObject.entrySet().forEach(this::handleDeviceEntry);
        eventBus.post(new ConfiguredEvent());
    }

    private void handleDeviceEntry(Map.Entry<String, JsonElement> entry) {
        JsonObject deviceConfiguration = entry.getValue().getAsJsonObject();
        JsonArray protocolJsonArray = deviceConfiguration.get("protocol").getAsJsonArray();

        String name = entry.getKey();
        List<String> protocols = this.getProtocolNames(protocolJsonArray);

        DeviceDefinition deviceDefinition = new DeviceDefinition(name, protocols);
        deviceRegistry.registerDevice(deviceDefinition);
    }

    private List<String> getProtocolNames(JsonArray protocolJsonArray) {
        List<String> protocolNames = new ArrayList<>();
        protocolJsonArray.forEach(jsonElement -> protocolNames.add(jsonElement.getAsString()));
        return protocolNames;
    }

}
