package de.jpilight.core.json;

import de.jpilight.core.json.SendableJsonModel;

import java.util.Map;

public class ControlJsonModel extends SendableJsonModel {

    public final String action = "control";

    public Map<String, Object> code;

}
