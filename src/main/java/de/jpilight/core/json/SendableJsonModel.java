package de.jpilight.core.json;

import com.google.inject.Inject;
import de.jpilight.core.connection.Connection;

public abstract class SendableJsonModel extends AbstractJsonModel {

    @Inject
    protected transient Connection connection;

    public void send() {
        connection.write(this.toJson());
    }

}
