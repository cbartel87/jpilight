package de.jpilight.core.json;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.inject.Inject;
import de.jpilight.core.bus.Bus;
import de.jpilight.core.event.Event;
import de.jpilight.core.json.JsonMessageHandler;
import de.jpilight.core.device.DeviceUpdateEvent;

import java.util.Map;
import java.util.stream.Collectors;

public class UpdateMessageHandler implements JsonMessageHandler {

    @Inject
    private Bus<Event> eventBus;

    @Override
    public boolean canHandle(JsonObject object) {
        if(!object.has("origin")) {
            return false;
        }
        JsonElement jsonElement = object.get("origin");
        if(!jsonElement.isJsonPrimitive()) {
            return false;
        }
        String origin = jsonElement.getAsString();
        return origin.equals("update");
    }

    @Override
    public void handle(JsonObject object) {
        object.get("devices").getAsJsonArray().forEach(element -> {

            String deviceName = element.getAsString();

            JsonObject values = object.get("values").getAsJsonObject();
            Map<String, String> updateValues = values.entrySet().stream()
                    .collect(Collectors.toMap(entry -> entry.getKey(), entry -> entry.getValue().getAsString()));

            eventBus.post(new DeviceUpdateEvent(deviceName, updateValues));

        });
    }


}
