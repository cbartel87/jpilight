package de.jpilight.core.json;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.google.gson.*;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import de.jpilight.core.bus.Bus;
import de.jpilight.core.connection.message.Message;
import de.jpilight.core.connection.message.RawMessage;

import java.util.Set;

public class JsonMessageHandlingService {

    private JsonParser jsonParser;
    private Set<JsonMessageHandler> jsonMessageHandler;

    @Inject
    public JsonMessageHandlingService(Bus<Message> messageBus, Set<JsonMessageHandler> jsonMessageHandler) {
        this.jsonMessageHandler = jsonMessageHandler;
        jsonParser = new JsonParser();
        messageBus.register(this);
    }

    @Subscribe
    public void onRawMessage(RawMessage message) {
        JsonElement jsonElement = jsonParser.parse(message.getText());
        if(jsonElement.isJsonObject()) {
            JsonObject jsonObject = jsonElement.getAsJsonObject();
            jsonMessageHandler.stream()
                    .filter(handler -> handler.canHandle(jsonObject))
                    .forEach(handler -> handler.handle(jsonObject));
        }
    }

}

