package de.jpilight.core.json;

import com.google.gson.Gson;

public abstract class AbstractJsonModel {

    protected static Gson gson = new Gson();

    public String toJson() {
        return gson.toJson(this);
    }

}
