package de.jpilight.core.json;

import de.jpilight.core.json.SendableJsonModel;

import java.util.HashMap;
import java.util.Map;

public class IdentifyJsonModel extends SendableJsonModel {

    public final String action = "identify";

    public final Map<String, Object> options = new HashMap<>();

}
