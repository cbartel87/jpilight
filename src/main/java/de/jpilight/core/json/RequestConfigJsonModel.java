package de.jpilight.core.json;

import de.jpilight.core.json.SendableJsonModel;

public class RequestConfigJsonModel extends SendableJsonModel {

    public final String action = "request config";

}
