package de.jpilight.core.json;

import com.google.gson.JsonObject;

public interface JsonMessageHandler {

    boolean canHandle(JsonObject object);
    void handle(JsonObject object);

}
