package de.jpilight.core;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;
import com.google.inject.TypeLiteral;
import com.google.inject.multibindings.Multibinder;
import de.jpilight.core.bus.Bus;
import de.jpilight.core.bus.BusImpl;
import de.jpilight.core.connection.Connection;
import de.jpilight.core.connection.JPilightSocketConnection;
import de.jpilight.core.event.Event;
import de.jpilight.core.connection.message.Message;
import de.jpilight.core.json.JsonMessageHandler;
import de.jpilight.core.json.JsonMessageHandlingService;
import de.jpilight.core.device.DeviceFactory;
import de.jpilight.core.device.DeviceRegistry;
import de.jpilight.core.device.DeviceRegistryImpl;
import de.jpilight.core.device.DeviceUpdateEventReceiver;
import de.jpilight.core.connection.ConnectedEventReceiver;
import de.jpilight.core.device.DeviceControlEventReceiver;
import org.reflections.Reflections;

import java.util.Set;

public class JPilightModule extends AbstractModule {

    private Reflections reflections;

    public JPilightModule() {
        this.reflections = new Reflections("de.jpilight");
    }

    @Override
    protected void configure() {
        bind(Connection.class).to(JPilightSocketConnection.class).in(Singleton.class);
        bindBusses();
        bindJsonMessageHandler();
        bindDeviceFactories();
        bind(DeviceRegistry.class).to(DeviceRegistryImpl.class).in(Singleton.class);
        bind(DeviceUpdateEventReceiver.class).asEagerSingleton();
        bind(DeviceControlEventReceiver.class).asEagerSingleton();
        bind(JsonMessageHandlingService.class).asEagerSingleton();
        bind(ConnectedEventReceiver.class).asEagerSingleton();

    }

    private void bindBusses() {
        bind(new TypeLiteral<Bus<Message>>() {}).to(new TypeLiteral<BusImpl<Message>>() {}).in(Singleton.class);
        bind(new TypeLiteral<Bus<Event>>() {}).to(new TypeLiteral<BusImpl<Event>>() {}).in(Singleton.class);
    }

    private void bindJsonMessageHandler() {
        Multibinder<JsonMessageHandler> jsonMessageHandlerMultibinder = Multibinder.newSetBinder(binder(), JsonMessageHandler.class);
        Set<Class<? extends JsonMessageHandler>> jsonMessageHandler = reflections.getSubTypesOf(JsonMessageHandler.class);
        jsonMessageHandler.forEach(handler -> jsonMessageHandlerMultibinder.addBinding().to(handler));
    }

    private void bindDeviceFactories() {
        Multibinder<DeviceFactory> deviceFactoryMultibinder = Multibinder.newSetBinder(binder(), DeviceFactory.class);
        Set<Class<? extends DeviceFactory>> deviceFactories = reflections.getSubTypesOf(DeviceFactory.class);
        deviceFactories.forEach(factory -> deviceFactoryMultibinder.addBinding().to(factory));
    }



}
