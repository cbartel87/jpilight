package de.jpilight.core.bus;

import com.google.common.eventbus.EventBus;

public class BusImpl<T> implements Bus<T> {

    private EventBus eventBus = new EventBus();

    @Override
    public void register(Object listener) {
        eventBus.register(listener);
    }

    @Override
    public void unregister(Object listener) {
        eventBus.unregister(listener);
    }

    @Override
    public void post(T object) {
        eventBus.post(object);
    }

    @Override
    public String toString() {
        return eventBus.toString();
    }

}
