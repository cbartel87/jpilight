package de.jpilight.core.bus;

public interface Bus<T> {
    void register(Object listener);

    void unregister(Object listener);

    void post(T object);
}
