package de.jpilight.util;

import de.jpilight.api.exception.JPilightException;

public class Blocker {

    public void block(long timeout) throws JPilightException {
        synchronized (this) {
            try {
                this.wait(timeout);
            } catch (InterruptedException e) {
                throw new JPilightException(e);
            }
        }
    }

    public void unblock() {
        synchronized (this) {
            this.notifyAll();
        }
    }
}
