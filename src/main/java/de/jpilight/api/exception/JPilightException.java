package de.jpilight.api.exception;

public class JPilightException extends Exception {

    public JPilightException() {
    }

    public JPilightException(String message) {
        super(message);
    }

    public JPilightException(String message, Throwable cause) {
        super(message, cause);
    }

    public JPilightException(Throwable cause) {
        super(cause);
    }

    public JPilightException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
