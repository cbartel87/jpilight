package de.jpilight.api;

import com.google.common.eventbus.Subscribe;
import com.google.inject.Guice;
import com.google.inject.Inject;
import de.jpilight.api.exception.JPilightException;
import de.jpilight.core.connection.event.ConfiguredEvent;
import de.jpilight.core.JPilightModule;
import de.jpilight.core.bus.Bus;
import de.jpilight.core.connection.Connection;
import de.jpilight.core.connection.event.DisconnectedEvent;
import de.jpilight.core.device.Device;
import de.jpilight.core.device.DeviceRegistry;
import de.jpilight.core.event.Event;
import de.jpilight.util.Blocker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Collection;

/**
 * The JPilightConfiguration class is the main programming interface for JPilight.
 */
public abstract class JPilightConfiguration {

    private final static Logger LOGGER = LoggerFactory.getLogger(JPilightConfiguration.class);

    @Inject
    private Connection connection;

    @Inject
    private Bus<Event> eventBus;

    @Inject
    private DeviceRegistry deviceRegistry;

    private boolean initialized = false;
    private boolean configured;
    private long timeoutInMilliseconds = 60000;
    private Blocker blocker = new Blocker();

    /**
     * Connect to the given Pilight Socket. If this configuration was not initialized before then init()
     * will be called first.
     * The configuration is automatically requested and parsed. After everything is configured onConnected()
     * will be called.
     *
     * @param hostname hostname of the machine which runs pilight
     * @param port port of the machine which runs pilight
     * @param blocking if set to false this call will not block
     * @throws JPilightException if anything goes wrong
     */
    public void connect(String hostname, int port, boolean blocking) throws JPilightException {
        this.configured = false;
        if(!initialized) {
            this.init();
        }
        try {
            connection.connect(hostname, port);
            if(blocking) {
                this.blocker.block(timeoutInMilliseconds);
                if(!configured) {
                    throw new JPilightException("Could not connect and configure within timeout. Try setting the timeout to a larger value");
                }
            }
        } catch (IOException e) {
            throw new JPilightException("Could not connect", e);
        }
    }

    /**
     * Connect to the given Pilight Socket. If this configuration was not initialized before then init()
     * will be called first.
     * The configuration is automatically requested and parsed. After everything is configured onConnected()
     * will be called.
     *
     * @param hostname hostname of the machine which runs pilight
     * @param port port of the machine which runs pilight
     * @throws JPilightException if anything goes wrong
     */
    public void connect(String hostname, int port) throws JPilightException {
        this.connect(hostname, port, true);
    }

    /**
     * Close the current connection.
     */
    public void disconnect() {
        this.connection.close();
    }

    /**
     * Initialize this configuration. This method will be called automatically if you call connect.
     * You can call this method if you wish to initialize your configuration earlier before you call the
     * connect method.
     */
    public void init() {
        if(initialized) {
            LOGGER.warn("already initialized, skipping...");
            return;
        }
        Guice.createInjector(new JPilightModule()).injectMembers(this);
        eventBus.register(this);
        initialized = true;
    }

    /**
     * Get the device with the given name
     * @param name the name of the device as configured in pilight
     * @param deviceType the device type
     * @return returns the device with the given name in an instance of the given deviceType
     */
    public <T extends Device> T getDevice(String name, Class<T> deviceType) {
        return deviceRegistry.getDevice(name, deviceType);
    }

    @Subscribe
    private void onConfiguredEvent(ConfiguredEvent event) {
        this.onConnected(this.deviceRegistry.getAllDevices());
        this.configured = true;
        this.blocker.unblock();
    }

    /**
     * This method will be called if you have successfully connected to the pilight socket and received its
     * configuration. All detected (and supported) devices are in the given collection of devices.
     * @param devices A Collection of all detected supported devices in your pilight configuration
     */
    protected abstract void onConnected(Collection<Device> devices);

    @Subscribe
    private void onDisconnectedEvent(DisconnectedEvent event) {
        this.onDisconnected();
    }

    /**
     * This method is called on disconnect from the pilight socket. You can use it to reconnect to pilight.
     */
    protected abstract void onDisconnected();

    /**
     *
     * @return The timeout in milliseconds if connecting in blocking mode.
     */
    public long getTimeoutInMilliseconds() {
        return timeoutInMilliseconds;
    }

    /**
     * If connecting in blocking mode this timeout is used.
     * @param timeoutInMilliseconds
     */
    public void setTimeoutInMilliseconds(long timeoutInMilliseconds) {
        this.timeoutInMilliseconds = timeoutInMilliseconds;
    }
}
