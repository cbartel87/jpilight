package de.jpilight.api.device;

import de.jpilight.core.device.Device;

import java.util.Map;

/**
 * The TemperatureSensor is a unit to measure, who would have guessed, temperatures.
 */
public class    TemperatureSensor extends Device<Double> {

    private Double temperature;

    @Override
    protected void update(Map<String, String> values) {
        Double temperature = Double.valueOf(values.get("temperature"));
        this.setTemperature(temperature);
        this.getObservers().forEach(observer -> observer.accept(temperature));
    }

    protected void setTemperature(Double temperature) {
        this.temperature = temperature;
    }

    /**
     * Get the current temperature
     * @return the current temperature
     */
    public Double getTemperature() {
        return temperature;
    }

}
