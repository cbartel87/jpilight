package de.jpilight.api.device;

import de.jpilight.core.device.ControlableDevice;

import java.util.Map;

/**
 * The RCSwitch is a remote controlled unit that can be turned of and on.
 */
public class RCSwitch extends ControlableDevice<Boolean> {

    private Boolean state;

    @Override
    protected void update(Map<String, String> values) {
        Boolean state = values.get("state").equals("on");
        this.setState(state);
        this.getObservers().forEach(observer -> observer.accept(state));
    }

    /**
     * Get the current state of this unit
     * @return true if the unit is turned on, false if it is turned off
     */
    public Boolean getState() {
        return state;
    }

    protected void setState(Boolean state) {
        this.state = state;
    }

    /**
     * turn this unit on. If the command was successful the state of this unit will change to true
     */
    public void turnOn() {
        this.sendControl("on");
    }

    /**
     * turn this unit off. If the command was successful the state of this unit will change to false
     */
    public void turnOff() {
        this.sendControl("off");
    }

}
