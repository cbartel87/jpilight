package de.jpilight.api;

import de.jpilight.api.device.TemperatureSensor;
import de.jpilight.core.device.Device;

import java.util.Collection;

public class JPilightConfigurationTest extends JPilightConfiguration {

    public static void main(String[] args) throws Exception {
        JPilightConfigurationTest test = new JPilightConfigurationTest();

        test.connect("192.168.1.51", 5000);

        Thread.sleep(5000);
        test.disconnect();
        Thread.sleep(1000);
    }

    @Override
    protected void onConnected(Collection<Device> devices) {
        System.out.println("detected devices: " + devices);
        TemperatureSensor temperatur = this.getDevice("Temperatur", TemperatureSensor.class);
        temperatur.addObserver(t -> System.out.println(t));
    }

    @Override
    protected void onDisconnected() {
        System.out.println("Disconnected!");
    }

}
