package de.jpilight.core.connection;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import de.jpilight.core.bus.BusImpl;
import de.jpilight.core.bus.Bus;
import de.jpilight.core.connection.event.ConnectedEvent;
import de.jpilight.core.connection.message.RawMessage;

import java.util.Scanner;

public class ManualConnectionTest {

    public static void main(String[] args) throws Exception{
        Connection connection = new SocketConnection();
        ManualConnectionTest manualConnectionTest = new ManualConnectionTest();
        connection.addObserver(manualConnectionTest::handleMessage);
        connection.connect("raspberrypi", 5000);
        System.out.println("connected!");

        Scanner scanner = new Scanner(System.in);
        while(true) {
            String input = scanner.nextLine();
            System.out.println("sending: " + input);
            connection.write(input);
        }
    }

    private void handleMessage(String msg) {
        System.out.println(msg);
    }

}
