package de.jpilight.core.connection;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

import de.jpilight.core.connection.channel.SocketReadChannel;
import de.jpilight.core.connection.channel.SocketWriteChannel;
import de.jpilight.core.bus.Bus;
import de.jpilight.core.event.Event;
import de.jpilight.core.connection.message.Message;
import de.jpilight.core.connection.message.RawMessage;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class JPilightSocketConnectionTest {

    private JPilightSocketConnection connection;

    @Mock
    private Bus<Message> messageBus;

    @Mock
    private Bus<Event> eventBus;

    @Mock
    private SocketWriteChannel socketWriteChannel;

    @Mock
    private SocketReadChannel socketReadChannel;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        connection = new JPilightSocketConnection();
        connection.messageBus = messageBus;
        connection.eventBus = eventBus;
        connection.socketWriteChannel = socketWriteChannel;
        connection.socketReadChannel = socketReadChannel;
    }

    @Test
    public void testHandleMessage() {
        String testMessage = "TESTMESSAGE";
        String delimiterMessage = "";
        connection.handleChunk(testMessage);
        connection.handleChunk(delimiterMessage);
        ArgumentCaptor<RawMessage> captor = ArgumentCaptor.forClass(RawMessage.class);
        verify(messageBus).post(captor.capture());
        RawMessage incomingMessage = captor.getValue();
        assertEquals(testMessage, incomingMessage.getText());
    }

}
