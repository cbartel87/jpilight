package de.jpilight.core.connection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

import static org.junit.Assert.assertEquals;

public class SocketConnectionIntegrationTest {

    private SocketConnection connection;

    private ServerSocket serverSocket;

    private Scanner in;
    private PrintWriter out;


    @Before
    public void init() throws Exception {
        serverSocket = new ServerSocket(1337);
        connection = new SocketConnection();

        Thread serverThread = new Thread(this::handleConnection);
        serverThread.start();
    }

    private void handleConnection() {
        try {
            Socket socket = serverSocket.accept();
            this.in = new Scanner(socket.getInputStream());
            this.out = new PrintWriter(socket.getOutputStream());
        } catch(Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void testWrite() throws Exception {
        connection.connect(new InetSocketAddress("localhost", 1337));
        Thread.sleep(1000);
        String testMessage = "Testmessage";
        connection.write(testMessage);
        String read = in.nextLine();
        assertEquals(testMessage, read);
    }

    @Test
    public void testRead() throws Exception {
        final String messageContent = "Testmessage";
        connection.addObserver((message -> {
            if(message == null) {
                return;
            }
            assertEquals(messageContent, message);
        }));
        connection.connect(new InetSocketAddress("localhost", 1337));
        Thread.sleep(1000);

        out.println(messageContent);
        out.flush();

        Thread.sleep(1000);
    }

    @After
    public void dispose() throws Exception {
        connection.close();
        serverSocket.close();
    }

}
