package de.jpilight.core.json;

import de.jpilight.core.json.RequestConfigJsonModel;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class RequestConfigJsonModelTest {

    private RequestConfigJsonModel model;

    @Before
    public void init() {
        model = new RequestConfigJsonModel();
    }

    @Test
    public void testToJson() {
        String expected = "{\"action\":\"request config\"}";
        assertEquals(expected, model.toJson());
    }

}
