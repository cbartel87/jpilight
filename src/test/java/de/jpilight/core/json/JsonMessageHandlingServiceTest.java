package de.jpilight.core.json;

import static org.junit.Assert.*;

import com.google.gson.JsonObject;
import de.jpilight.TestMessages;
import de.jpilight.core.bus.Bus;
import de.jpilight.core.connection.message.Message;
import de.jpilight.core.connection.message.RawMessage;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Collections;

public class JsonMessageHandlingServiceTest {

    private JsonMessageHandlingService service;

    @Mock
    private Bus<Message> messageBus;

    @Mock
    private JsonMessageHandler jsonMessageHandler;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        service = new JsonMessageHandlingService(messageBus, Collections.singleton(jsonMessageHandler));
    }

    @Test
    public void testOnIncomingRawMessage() {
        RawMessage message = new RawMessage(TestMessages.SUCCESS);
        Mockito.when(jsonMessageHandler.canHandle(Mockito.anyObject())).thenReturn(true);
        ArgumentCaptor<JsonObject> argumentCaptor = ArgumentCaptor.forClass(JsonObject.class);
        service.onRawMessage(message);
        Mockito.verify(jsonMessageHandler).canHandle(Mockito.anyObject());
        Mockito.verify(jsonMessageHandler).handle(argumentCaptor.capture());
        JsonObject jsonObject = argumentCaptor.getValue();
        assertEquals(TestMessages.SUCCESS, jsonObject.toString());
    }

}
