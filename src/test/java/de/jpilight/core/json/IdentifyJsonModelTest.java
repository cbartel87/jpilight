package de.jpilight.core.json;

import static org.junit.Assert.*;

import de.jpilight.core.json.IdentifyJsonModel;
import org.junit.Before;
import org.junit.Test;

public class IdentifyJsonModelTest {

    private IdentifyJsonModel model;

    @Before
    public void init() {
        model = new IdentifyJsonModel();
    }

    @Test
    public void testToJson() {
        String expected = "{\"action\":\"identify\",\"options\":{}}";
        assertEquals(expected, model.toJson());
    }

}
