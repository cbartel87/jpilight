package de.jpilight;

import java.util.Scanner;

public class TestMessages {

    public static final String CONFIGURATION
            = new Scanner(TestMessages.class.getResourceAsStream("configuration.json"), "UTF-8")
            .useDelimiter("\\A")
            .next();

    public static final String SUCCESS
            = new Scanner(TestMessages.class.getResourceAsStream("success.json"), "UTF-8")
            .useDelimiter("\\A")
            .next();

    public static final String FAILURE
            = new Scanner(TestMessages.class.getResourceAsStream("failure.json"), "UTF-8")
            .useDelimiter("\\A")
            .next();

}
