package de.jpilight;

import com.google.inject.Guice;
import com.google.inject.Injector;
import de.jpilight.core.JPilightModule;
import de.jpilight.core.connection.Connection;
import de.jpilight.core.device.DeviceRegistry;
import de.jpilight.api.device.RCSwitch;
import de.jpilight.api.device.TemperatureSensor;

public class TestStarter {

    public static void main(String[] args) throws Exception {
        Injector injector = Guice.createInjector(new JPilightModule());


        Connection connection = injector.getInstance(Connection.class);
        connection.connect("192.168.1.51", 5000);

        Thread.sleep(500);

        DeviceRegistry deviceRegistry = injector.getInstance(DeviceRegistry.class);
        TemperatureSensor temperatur = deviceRegistry.getDevice("Temperatur", TemperatureSensor.class);
        temperatur.addObserver(t -> System.out.println(t));

        RCSwitch einkocher = deviceRegistry.getDevice("Einkocher", RCSwitch.class);
        einkocher.addObserver(s -> System.out.println(s));
        einkocher.turnOn();
        Thread.sleep(500);
        einkocher.turnOff();


        Thread.sleep(5000);
        connection.close();
    }
}
