# JPilight #

### License ###
Copyright 2017 JPilight
[Licensed under the Apache License, Version 2.0](https://bitbucket.org/cbartel87/jpilight/raw/master/license.txt)


### What is this? ###

JPilight connects from Java to the Pilight socket. It makes working with Pilight from local or remote a breeze as it automatically detects your configured devices and creates appropriate Java Objects that are easy to use.


### How to use? ###

Simply extend `JPilightConfiguration` and implement the methods as you would like them to be. You can then use the instance of your JPilightConfiguration in any way you want. For example to control your AC


```java
public class MyJPilightConfiguration extends JPilightConfiguration {

    @Override
    protected void onConnected(Collection<Device> devices) {
        System.out.println("detected devices: " + devices);
		
		TemperatureSensor temperature = this.getDevice("Living-Room", TemperatureSensor.class);
		RCSwitch rcSwitch = this.getDevice("Living-Room-AC");
		temperature.addObserver(t -> {
			if (t > 21.5) {
				rcSwitch.turnOn();
			} else {
				rcSwitch.turnOff();
			}
		});
    }

    @Override
    protected void onDisconnected() {
        System.out.println("Disconnected!");
    }

}
```
Then connect

```java
	JPilightConfiguration jpilight = new MyJPilightConfiguration();
	jpilight.connect("192.168.1.2", 5000);
```